// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};
var app = {};
var fontawesome = require('IconicFont').IconicFont({
        font: 'FontAwesome',
        ligature: false        // optional
});

//
// Icons
//
Alloy.Globals.icon_font = fontawesome.fontfamily(); 
Alloy.Globals.icon_back = fontawesome.icon('icon-chevron-left');
Alloy.Globals.icon_search = fontawesome.icon('icon-search');
Alloy.Globals.icon_book = fontawesome.icon('icon-book');
Alloy.Globals.icon_play_circle = fontawesome.icon('icon-play-circle');
Alloy.Globals.icon_comment = fontawesome.icon('icon-comment');
Alloy.Globals.icon_desktop = fontawesome.icon('icon-desktop');
Alloy.Globals.icon_globe = fontawesome.icon('icon-globe');
Alloy.Globals.icon_share = fontawesome.icon('icon-share');

Alloy.Globals.TwitterConsumerSecret = 'l183kstis72xlWdJkEgYAG4W0Ffli7PWyxZJjRqdkw';
Alloy.Globals.TwitterConsumerKey = 'krROZoQVQdrZ1L6eKgdoDA';

Alloy.Globals.mask = Alloy.createController('mask').getView();
Alloy.Globals.modal = Alloy.createController('modalShare').getView();

//
// Animations
//
Alloy.Globals.slide_up_from_bottom = Ti.UI.createAnimation({
  bottom: 0,
  duration: 300 
});

Alloy.Globals.slide_down_to_bottom = Ti.UI.createAnimation({
  bottom: -200,
  duration: 300 
});

//
// Other global variables
//
Alloy.Globals.site_url = 'http://nationaldb.org';

app.openSearch = function() 
{
  Alloy.createController('search').getView().open();
};

app.modalControl = {
  openShare: function() {
    Alloy.Globals.mask.open();
    Alloy.Globals.modal.open(Alloy.Globals.slide_up_from_bottom);
  }
};

app.goBack = function() 
{
  Alloy.Globals.navigation.back();
};
