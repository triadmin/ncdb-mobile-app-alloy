//
// Spin up our navigation controller
//
var navigation = Alloy.Globals.navigation = Alloy.createController("navigation");

var conf = {
	mainWindow: undefined,
	index: 'libraryCatsList',
	indexOptions: {
		transition: 'none'	
	},
	historyLimit: 20,
	defaultOpenTransition: {transition: 'none', duration: 150, transitionColor: "#fff"}, 
	defaultBackTransition: {transition: 'none', duration: 150, transitionColor: "#fff"},
	confirmOnExit: true
};

if (OS_IOS) {
	conf.defaultOpenTransition = {transition: 'slideInFromRight', duration: 150, transitionColor: "#fff"}; 
	conf.defaultBackTransition = {transition: 'slideInFromLeft', duration: 150, transitionColor: "#fff"};
}

navigation.init(conf);

