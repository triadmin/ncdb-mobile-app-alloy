var args = arguments[0] || {};
//
// this is setting the view elements of the row view
// which is found in views/rowLibraryCats.xml based on the arguments
// passed into the controller
//
$.libraryCat.text = args.libraryCat || '';

if ( args.subCat == true )
{
	var style = $.createStyle({
		classes: 'table-row-sub-label'
	});
	$.libraryCat.applyProperties( style );
}
