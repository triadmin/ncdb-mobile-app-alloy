var ag = Alloy.Globals;
var args = arguments[0] || {};
var LibraryView = {};

LibraryView.init = function()
{
  ag.ActiveLibraryURL = args.libraryItemEmailURL;
  if (args.libraryItemURL != '')
  {
    $.webLibraryItem.url = args.libraryItemURL;
  } else {
    $.webLibraryItem.html = args.libraryContent;
  }
};

LibraryView.init();
