var ag = Alloy.Globals;
var ModalShare = {};

function modalClose() 
{
  $.winModal.close(ag.slide_down_to_bottom);
  ag.mask.close();
}

ModalShare.init = function()
{
  $.btn_mail.addEventListener('click', function() {
    Alloy.createController('emailDialog', {
      messageBody: 'Hello. I thought you might enjoy this link from the NCDB Library. \n------------------\n' + ag.ActiveLibraryURL
    }).getView().open();
  });
  
  $.btn_fb.addEventListener('click', function() {
    
  });
  
  $.btn_twitter.addEventListener('click', function() {
    Ti.API.info('auth: ' + social.isAuthorized());
    if ( !social.isAuthorized() )
    {
      social.authorize();
    } else {
      social.share({
        message: 'Hey there',
        success: function(e) { alert('Success!'); },
        error: function(e) { alert('Error!'); }
      });
    }  
  });
  
  var social = require('alloy/social').create({
    site: 'twitter',
    consumerSecret: ag.TwitterConsumerSecret,
    consumerKey: ag.TwitterConsumerKey
  });
};

ModalShare.init();


