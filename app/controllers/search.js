var ag = Alloy.Globals;
var Search = {};

Search.init = function()
{
  $.txtSearch.focus();
  var lastSearch = null, timer;
  $.txtSearch.addEventListener('change', function(e) {
    if ($.txtSearch.value.length > 2 && $.txtSearch.value != lastSearch) {     
      clearTimeout(timer);
        timer = setTimeout(function() {
          lastSearch = $.txtSearch.value;
          Search.getData( $.txtSearch.value );
        }, 300);
        
      }
      return false;
  });
  Search.events.attachRowEvents();
};

Search.getData = function(strSearch)
{
  var AJAX = Ti.Network.createHTTPClient();
  AJAX.onload = function() {
    var rowData = [];
    var jdata = JSON.parse(this.responseText);
    var data = [];
    _.each(jdata.data, function(element, index, list) {
      var row = Alloy.createController( 'rowSearch', {
                    libraryTitle: element.LibraryTitle
            }).getView();
            row.libraryItemURL = element.LibraryLink;
            if (element.LibraryBody != '')
            {
              var libraryBody = '<style>* { font-family: "Helvetica Neue"; } h1,h1 { font-family: "Times New Roman" }</style><h1>' + element.LibraryTitle + '</h1>' + element.LibraryBody;
            }
            row.libraryContent = libraryBody;
            data.push(row);
      
    }); 
    $.tblSearchResults.setData(data);
  };
  AJAX.open('GET',ag.site_url + '/api/v1/library?search=' + strSearch + '&select=LibraryTitle,LibraryId,LibraryLink,LibraryBody');
  AJAX.send();  
};

Search.events = {
  attachRowEvents : function()
  {
    $.tblSearchResults.addEventListener('click', function(_e) {
      closeSearchWindow();
      ag.navigation.open('libraryItemView', {
        libraryItemURL: _e.row.libraryItemURL,
        libraryContent: _e.row.libraryContent
      });
    });
  }
};

function closeSearchWindow()
{
  $.searchWindow.close();
}

Search.init();

