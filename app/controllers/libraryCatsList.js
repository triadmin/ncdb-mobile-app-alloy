var ag = Alloy.Globals;
LibraryCats = {};

LibraryCats.init = function()
{
  //
  // Turn off the back button in the header.
  //
  var btn_back = $.headerLibrary.getView('btn_back');
  btn_back.visible = false;
  
  //
  // Get library categories
  //
  var libraryCats = Alloy.createCollection( 'libraryCatsList' );
  libraryCats.fetch({
  success: function() {
    var data = [];
    
    _.each(libraryCats.models, function(element, index, list) {
      var a = element.attributes;     
      var row = Alloy.createController( 'rowLibraryCats', {
                    libraryCat: a['LibraryCategoriesTitle'],
                    subCat: false
            }).getView();
            row.librarySubCats = a['LibrarySubCategories'];
            row.subCatOpen = false;
            row.subCat = false;
            data.push(row);
        });
        $.tblLibraryCats.setData(data);
        $.tblLibraryCats.addEventListener('click', function(_e) {
          LibraryCats.utils.deleteSubCatRows();
          if ( _e.row.subCatOpen == false ) {
            var row_index = _e.index;
            _e.row.subCatOpen = true;
            _.each( _e.row.librarySubCats, function(element) {
              var row = Alloy.createController('rowLibraryCats', {
                      libraryCat: element.LibraryCategoriesTitle,
                      subCat: true
                }).getView();
              row.subCat = true;
              row.subCatId = element.LibraryCategoriesId; 
              $.tblLibraryCats.insertRowAfter( row_index, row, {animationStyle:Titanium.UI.iPhone.RowAnimationStyle.LEFT} );
            });
          } 
          
          if ( _e.row.subCat == true ) {
            ag.navigation.open('libraryList', {
              subCatId: _e.row.subCatId
            });
          }
        });
    },
    error: function() {
      Ti.API.error("hmm - this is not good!");
    }
  });
};

LibraryCats.utils = {
  deleteSubCatRows : function() {
    var rows = $.tblLibraryCats.data[0].rows;
      for (var i = 0; i < rows.length - 1; i++) {
        if ( rows[i].subCat == true ) {
          Ti.API.info('subCat');
          //$.tblLibraryCats.deleteRow(i);
        }
      };
  }
};

LibraryCats.init();




