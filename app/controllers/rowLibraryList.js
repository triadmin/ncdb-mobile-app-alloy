var args = arguments[0] || {};
//
// this is setting the view elements of the row view
// which is found in views/rowLibraryCats.xml based on the arguments
// passed into the controller
//
$.libraryItem.text = args.libraryTitle || '';

var iconStyle = $.createStyle({
  classes: args.libraryFormatIcon
});
$.libraryItemIcon.applyProperties( iconStyle );
