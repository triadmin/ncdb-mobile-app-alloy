var ag = Alloy.Globals;
var args = arguments[0] || {};
var LibraryList = {};

LibraryList.init = function()
{
  libraryList = Alloy.createCollection('libraryList', { subCatId: args.subCatId } );
  libraryList.fetch({
    success: function() {
      LibraryList.ui.renderRows();
    },
    error: function() {
      Ti.API.error("hmm - this is not good!");
    }
  });
};

LibraryList.ui = {
  renderRows : function()
  {
    var data = [];
    _.each(libraryList.models, function(element, index, list) {
      var a = element.attributes;  
      var row = Alloy.createController( 'rowLibraryList', {
                    libraryTitle: a['LibraryTitle'],
                    libraryFormatIcon: a['LibraryFormatIcon'][0]
            }).getView();
            row.libraryItemURL = a['LibraryLink'];
            row.libraryItemEmailURL = a['LibraryLink'];
            if (a['LibraryBody'] != '')
            {
              row.libraryItemEmailURL = ag.site_url + '/library/page/' + a['LibraryId'];
              var libraryBody = '<style>* { font-family: "Helvetica Neue"; } h1,h1 { font-family: "Times New Roman" }</style><h1>' + a['LibraryTitle'] + '</h1>' + a['LibraryBody'];
            }
            row.libraryContent = libraryBody;
            data.push(row);
    });
    $.tblLibraryList.setData(data);
    LibraryList.events.attachRowEvents();
  }
};

LibraryList.events = {
  attachRowEvents : function()
  {
    $.tblLibraryList.addEventListener('click', function(_e) {
      ag.navigation.open('libraryItemView', {
        libraryItemURL: _e.row.libraryItemURL,
        libraryContent: _e.row.libraryContent,
        libraryItemEmailURL: _e.row.libraryItemEmailURL
      });
    });
  }
};

LibraryList.init();  