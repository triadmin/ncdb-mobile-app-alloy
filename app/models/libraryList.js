var ag = require('alloy').Globals;

exports.definition = {
	config: {
        'debug': true, 
        'adapter': {
            'type': 'restapi',
            'collection_name': 'libraryList',
            'idAttribute': 'id'
        },        
        'parentNode': 'data'
    },      
    extendModel: function(Model) {      
        _.extend(Model.prototype, {});
        return Model;
    },  
    extendCollection: function(Collection) {  
        _.extend(Collection.prototype, {
          initialize: function(data) {
            this.config.URL = ag.site_url + '/api/v1/library/list/' + data.subCatId;
            Ti.API.info(this.config);
          }
        });
        return Collection;
    }       
};
