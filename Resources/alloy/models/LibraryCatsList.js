var ag = require("alloy").Globals;

exports.definition = {
    config: {
        URL: ag.site_url + "/api/v1/library/categories",
        debug: true,
        adapter: {
            type: "restapi",
            collection_name: "libraryCatsList",
            idAttribute: "id"
        },
        parentNode: "data"
    },
    extendModel: function(Model) {
        _.extend(Model.prototype, {});
        return Model;
    },
    extendCollection: function(Collection) {
        _.extend(Collection.prototype, {});
        return Collection;
    }
};

var Alloy = require("alloy"), _ = require("alloy/underscore")._, model, collection;

model = Alloy.M("libraryCatsList", exports.definition, []);

collection = Alloy.C("libraryCatsList", exports.definition, model);

exports.Model = model;

exports.Collection = collection;