function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "libraryCatsList";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.libraryCatsWindow = Ti.UI.createView({
        id: "libraryCatsWindow"
    });
    $.__views.libraryCatsWindow && $.addTopLevelView($.__views.libraryCatsWindow);
    $.__views.headerLibrary = Alloy.createController("includes/headerLibrary", {
        id: "headerLibrary",
        __parentSymbol: $.__views.libraryCatsWindow
    });
    $.__views.headerLibrary.setParent($.__views.libraryCatsWindow);
    $.__views.tblLibraryCats = Ti.UI.createTableView({
        top: 60,
        id: "tblLibraryCats"
    });
    $.__views.libraryCatsWindow.add($.__views.tblLibraryCats);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var ag = Alloy.Globals;
    LibraryCats = {};
    LibraryCats.init = function() {
        var btn_back = $.headerLibrary.getView("btn_back");
        btn_back.visible = false;
        var libraryCats = Alloy.createCollection("libraryCatsList");
        libraryCats.fetch({
            success: function() {
                var data = [];
                _.each(libraryCats.models, function(element) {
                    var a = element.attributes;
                    var row = Alloy.createController("rowLibraryCats", {
                        libraryCat: a["LibraryCategoriesTitle"],
                        subCat: false
                    }).getView();
                    row.librarySubCats = a["LibrarySubCategories"];
                    row.subCatOpen = false;
                    row.subCat = false;
                    data.push(row);
                });
                $.tblLibraryCats.setData(data);
                $.tblLibraryCats.addEventListener("click", function(_e) {
                    LibraryCats.utils.deleteSubCatRows();
                    if (false == _e.row.subCatOpen) {
                        var row_index = _e.index;
                        _e.row.subCatOpen = true;
                        _.each(_e.row.librarySubCats, function(element) {
                            var row = Alloy.createController("rowLibraryCats", {
                                libraryCat: element.LibraryCategoriesTitle,
                                subCat: true
                            }).getView();
                            row.subCat = true;
                            row.subCatId = element.LibraryCategoriesId;
                            $.tblLibraryCats.insertRowAfter(row_index, row, {
                                animationStyle: Titanium.UI.iPhone.RowAnimationStyle.LEFT
                            });
                        });
                    }
                    true == _e.row.subCat && ag.navigation.open("libraryList", {
                        subCatId: _e.row.subCatId
                    });
                });
            },
            error: function() {
                Ti.API.error("hmm - this is not good!");
            }
        });
    };
    LibraryCats.utils = {
        deleteSubCatRows: function() {
            var rows = $.tblLibraryCats.data[0].rows;
            for (var i = 0; rows.length - 1 > i; i++) true == rows[i].subCat && Ti.API.info("subCat");
        }
    };
    LibraryCats.init();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;