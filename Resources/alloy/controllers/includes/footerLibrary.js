function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "includes/footerLibrary";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.footerLibrary = Ti.UI.createView({
        bottom: 0,
        width: "100%",
        height: 40,
        backgroundColor: "#eee",
        id: "footerLibrary"
    });
    $.__views.footerLibrary && $.addTopLevelView($.__views.footerLibrary);
    $.__views.btn_share = Ti.UI.createButton({
        top: 5,
        right: 10,
        font: {
            fontFamily: Alloy.Globals.icon_font,
            fontSize: 20
        },
        color: "#ba594c",
        title: Alloy.Globals.icon_share,
        id: "btn_share"
    });
    $.__views.footerLibrary.add($.__views.btn_share);
    try {
        $.__views.btn_share.addEventListener("click", app.modalControl.openShare);
    } catch (e) {
        __defers["$.__views.btn_share!click!app.modalControl.openShare"] = true;
    }
    exports.destroy = function() {};
    _.extend($, $.__views);
    __defers["$.__views.btn_share!click!app.modalControl.openShare"] && $.__views.btn_share.addEventListener("click", app.modalControl.openShare);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;