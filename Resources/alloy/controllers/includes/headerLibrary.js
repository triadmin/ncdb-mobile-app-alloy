function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "includes/headerLibrary";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.headerLibrary = Ti.UI.createView({
        top: 0,
        width: "100%",
        height: 60,
        backgroundColor: "#3a1f21",
        id: "headerLibrary"
    });
    $.__views.headerLibrary && $.addTopLevelView($.__views.headerLibrary);
    $.__views.btn_back = Ti.UI.createButton({
        top: 28,
        left: 10,
        font: {
            fontFamily: Alloy.Globals.icon_font,
            fontSize: 20
        },
        color: "white",
        title: Alloy.Globals.icon_back,
        id: "btn_back"
    });
    $.__views.headerLibrary.add($.__views.btn_back);
    try {
        $.__views.btn_back.addEventListener("click", app.goBack);
    } catch (e) {
        __defers["$.__views.btn_back!click!app.goBack"] = true;
    }
    $.__views.btn_search = Ti.UI.createButton({
        top: 28,
        right: 10,
        font: {
            fontFamily: Alloy.Globals.icon_font,
            fontSize: 20
        },
        color: "white",
        title: Alloy.Globals.icon_search,
        id: "btn_search"
    });
    $.__views.headerLibrary.add($.__views.btn_search);
    try {
        $.__views.btn_search.addEventListener("click", app.openSearch);
    } catch (e) {
        __defers["$.__views.btn_search!click!app.openSearch"] = true;
    }
    $.__views.window_title = Ti.UI.createLabel({
        font: {
            fontFamily: "Palatino-Roman",
            fontSize: 22
        },
        top: 25,
        color: "white",
        text: "NCDB Library",
        id: "window_title"
    });
    $.__views.headerLibrary.add($.__views.window_title);
    exports.destroy = function() {};
    _.extend($, $.__views);
    __defers["$.__views.btn_back!click!app.goBack"] && $.__views.btn_back.addEventListener("click", app.goBack);
    __defers["$.__views.btn_search!click!app.openSearch"] && $.__views.btn_search.addEventListener("click", app.openSearch);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;