function Controller() {
    function modalClose() {
        $.winModal.close(ag.slide_down_to_bottom);
        ag.mask.close();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "modalShare";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.winModal = Ti.UI.createWindow({
        backgroundColor: "white",
        statusBarStyle: Titanium.UI.iPhone.StatusBar.LIGHT_CONTENT,
        height: 200,
        bottom: -200,
        opacity: ".9",
        zIndex: 100,
        id: "winModal"
    });
    $.__views.winModal && $.addTopLevelView($.__views.winModal);
    $.__views.__alloyId0 = Ti.UI.createView({
        id: "__alloyId0"
    });
    $.__views.winModal.add($.__views.__alloyId0);
    $.__views.btn_mail = Ti.UI.createButton({
        width: 64,
        height: 64,
        top: 10,
        left: 10,
        backgroundImage: "images/icons/email.png",
        id: "btn_mail"
    });
    $.__views.__alloyId0.add($.__views.btn_mail);
    $.__views.btn_fb = Ti.UI.createButton({
        width: 64,
        height: 64,
        top: 10,
        left: 84,
        backgroundImage: "images/icons/fb.png",
        id: "btn_fb"
    });
    $.__views.__alloyId0.add($.__views.btn_fb);
    $.__views.btn_twitter = Ti.UI.createButton({
        width: 64,
        height: 64,
        top: 10,
        left: 158,
        backgroundImage: "images/icons/twitter.png",
        id: "btn_twitter"
    });
    $.__views.__alloyId0.add($.__views.btn_twitter);
    $.__views.lblClose = Ti.UI.createLabel({
        top: 170,
        text: "Done",
        id: "lblClose"
    });
    $.__views.__alloyId0.add($.__views.lblClose);
    modalClose ? $.__views.lblClose.addEventListener("click", modalClose) : __defers["$.__views.lblClose!click!modalClose"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    var ag = Alloy.Globals;
    var ModalShare = {};
    ModalShare.init = function() {
        $.btn_mail.addEventListener("click", function() {
            Alloy.createController("emailDialog", {
                messageBody: "Hello. I thought you might enjoy this link from the NCDB Library. \n------------------\n" + ag.ActiveLibraryURL
            }).getView().open();
        });
        $.btn_fb.addEventListener("click", function() {});
        $.btn_twitter.addEventListener("click", function() {
            Ti.API.info("auth: " + social.isAuthorized());
            social.isAuthorized() ? social.share({
                message: "Hey there",
                success: function() {
                    alert("Success!");
                },
                error: function() {
                    alert("Error!");
                }
            }) : social.authorize();
        });
        var social = require("alloy/social").create({
            site: "twitter",
            consumerSecret: ag.TwitterConsumerSecret,
            consumerKey: ag.TwitterConsumerKey
        });
    };
    ModalShare.init();
    __defers["$.__views.lblClose!click!modalClose"] && $.__views.lblClose.addEventListener("click", modalClose);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;