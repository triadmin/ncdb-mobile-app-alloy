function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "navigation";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.appWrap = Ti.UI.createView({
        id: "appWrap"
    });
    $.__views.appWrap && $.addTopLevelView($.__views.appWrap);
    $.__views.content = Ti.UI.createView({
        id: "content"
    });
    $.__views.appWrap.add($.__views.content);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.prop = {};
    $.prop.historyStack = new Array();
    $.prop.historyStackOptions = new Array();
    $.prop.historyLimit = 10;
    $.prop.index = void 0;
    $.prop.indexOptions = void 0;
    $.prop.defaultOpenTransition = {
        transition: "none",
        transitionColor: "#fff",
        duration: 150
    };
    $.prop.defaultBackTransition = {
        transition: "none",
        transitionColor: "#000",
        duration: 150
    };
    $.prop.confirmOnExit = true;
    $.transitions = {};
    $.confirmedExit = false;
    $.mainWindow = void 0;
    $.previous = {
        controller: void 0,
        options: void 0,
        view: void 0
    };
    $.current = {
        controller: void 0,
        options: void 0,
        view: void 0
    };
    $.merge = function(mergeInto, mergeFrom) {
        var newObj = {};
        for (var prop in mergeInto) newObj[prop] = mergeInto[prop];
        for (var prop in mergeFrom) newObj[prop] = mergeFrom[prop];
        return newObj;
    };
    $.mergeMissing = function(mergeInto, mergeFrom) {
        var newObj = {};
        for (var prop in mergeInto) newObj[prop] = mergeInto[prop];
        for (var prop in mergeFrom) newObj.hasOwnProperty(prop) || (newObj[prop] = mergeFrom[prop]);
        return newObj;
    };
    exports.init = function(args) {
        Ti.API.info("Initializing application...");
        $.prop.historyStack = new Array();
        $.prop.historyStackOptions = new Array();
        if (args.mainWindow) {
            $.mainWindow = args.mainWindow;
            delete args.mainWindow;
        } else $.mainWindow = Ti.UI.createWindow({
            backgroundColor: "#000",
            navBarHidden: true,
            exitOnClose: true
        });
        for (var option in args) set(option, args[option]);
        $.mainWindow.add($.getView());
        $.mainWindow.open();
        exports.home();
        Ti.API.info("Application initialization complete");
    };
    exports.get = function(property) {
        return $.prop.hasOwnProperty(property) ? $.prop[property] : void 0;
    };
    var set = exports.set = function(property, value) {
        $.prop[property] = value;
    };
    exports.getMainWindow = function() {
        return $.mainWindow;
    };
    exports.open = function(controller, options, callback) {
        exports.fireEvent("openstart");
        if (!controller) {
            Ti.API.error("Open of view failed, no controller specified");
            return false;
        }
        options || (options = {});
        options = $.mergeMissing(options, $.prop.defaultOpenTransition);
        if ("string" == typeof controller) {
            Ti.API.info("Opening controller: " + controller + ", Options: " + JSON.stringify(options));
            controller = Alloy.createController(controller, options);
            controller.hasOwnProperty("init") && controller.init();
        } else Ti.API.info("Opening unknown controller view. Options: " + JSON.stringify(options));
        $.current.controller = controller;
        $.current.options = options;
        $.current.view = $.current.controller.getView();
        if ($.prop.historyStack.length > 0) {
            var prevIndex = $.prop.historyStack.length - 1;
            $.previous.controller = $.prop.historyStack[prevIndex];
            $.previous.options = $.prop.historyStackOptions[prevIndex];
            $.previous.view = $.previous.controller.getView();
        } else {
            $.previous.controller = void 0;
            $.previous.options = void 0;
            $.previous.view = void 0;
        }
        $.prop.historyStackOptions.push(options);
        $.prop.historyStack.push(controller);
        var transition = options.transition || void 0;
        if (!exports.hasTransition(transition)) {
            Ti.API.warn('The set transition "' + transition + '" doesn\'t exist, defaulting to "' + $.prop.defaultBackTransition.transition + '"');
            transition = $.prop.defaultBackTransition.transition;
        }
        $.transitions[transition]($.current.view, $.previous.view, options, function() {
            $.prop.historyLimit > 0 && exports.clearHistory($.prop.historyLimit);
            exports.fireEvent("opencomplete");
            callback && callback();
        });
    };
    exports.back = function(newOptions, callback) {
        newOptions || (newOptions = {});
        if (1 >= $.prop.historyStack.length) {
            Ti.API.info("At end of historyStack, closing application");
            exports.exit();
            return true;
        }
        Ti.API.info("Going back in the history stack");
        exports.fireEvent("backstart");
        $.previous.controller = $.prop.historyStack.pop();
        $.previous.options = $.prop.historyStackOptions.pop();
        $.previous.view = $.previous.options.hasOwnProperty("view") ? $.previous.options.view : $.previous.controller.getView();
        var curIndex = $.prop.historyStack.length - 1;
        $.current.controller = $.prop.historyStack[curIndex];
        $.current.options = $.prop.historyStackOptions[curIndex];
        $.current.view = $.current.options.hasOwnProperty("view") ? $.current.options.view : $.current.controller.getView();
        var options = $.merge($.current.options, $.prop.defaultBackTransition);
        options = $.merge(options, newOptions);
        var transition = options.transition || void 0;
        if (!exports.hasTransition(transition)) {
            Ti.API.warn('The set transition "' + transition + '" doesn\'t exist, defaulting to "' + $.prop.defaultBackTransition.transition + '"');
            transition = $.prop.defaultBackTransition.transition;
        }
        $.transitions[transition]($.current.view, $.previous.view, options, function() {
            $.previous.controller.destroy();
            $.previous.controller = void 0;
            $.previous.options = void 0;
            $.previous.view = void 0;
            exports.fireEvent("backcomplete");
            callback && callback();
        });
    };
    exports.home = function(options, callback) {
        options = options ? $.merge($.prop.indexOptions, options) : $.prop.indexOptions;
        exports.open($.prop.index, options, callback);
    };
    exports.addTransition = function(name, action) {
        $.transitions[name] = action;
    };
    exports.hasTransition = function(name) {
        return name ? $.transitions.hasOwnProperty(name) : false;
    };
    $.transitions.crossFade = function(newView, previousView, options, callback) {
        exports.fireEvent("transitionstart");
        if (previousView) {
            var oldOpacity = previousView.opacity || 1;
            var oldZIndex = previousView.zIndex || 0;
            previousView.zIndex = 9;
            $.content.add(newView);
            previousView.animate({
                opacity: 0,
                duration: options.duration
            }, function() {
                $.content.remove(previousView);
                previousView.opacity = oldOpacity;
                previousView.zIndex = oldZIndex;
                exports.fireEvent("transitionend");
                callback && callback();
            });
        } else {
            var opacity = newView.opacity || 1;
            newView.opacity = 0;
            $.content.add(newView);
            newView.animate({
                opacity: 1,
                duration: options.duration
            }, function() {
                newView.opacity = opacity;
                exports.fireEvent("transitionend");
                callback && callback();
            });
        }
    };
    $.transitions.fade = function(newView, previousView, options, callback) {
        exports.fireEvent("transitionstart");
        if (previousView) {
            var transitionColor = options.transitionColor ? options.transitionColor : "#000";
            var oldZIndex = previousView.zIndex || 0;
            var oldOpacity = previousView.opacity || 1;
            previousView.zIndex = 9;
            var transitionView = Ti.UI.createView({
                backgroundColor: transitionColor,
                height: $.appWrap.height,
                width: $.appWrap.width,
                left: 0,
                top: 0,
                zIndex: 8,
                opacity: 1
            });
            $.content.add(transitionView);
            $.content.add(newView);
            previousView.animate({
                opacity: 0,
                duration: Math.floor(options.duration / 2)
            }, function() {
                $.content.remove(previousView);
                previousView.opacity = oldOpacity;
                previousView.zIndex = oldZIndex;
                transitionView.animate({
                    opacity: 0,
                    duration: Math.floor(options.duration / 2)
                }, function() {
                    $.content.remove(transitionView);
                    delete transitionView;
                    exports.fireEvent("transitionend");
                    callback && callback();
                });
            });
        } else {
            var opacity = newView.opacity || 1;
            newView.opacity = 0;
            $.content.add(newView);
            newView.animate({
                opacity: opacity,
                duration: options.duration
            }, function() {
                newView.opacity = opacity;
                exports.fireEvent("transitionend");
                callback && callback();
            });
        }
    };
    $.transitions.slideInFromRight = function(newView, previousView, options, callback) {
        exports.fireEvent("transitionstart");
        if (previousView) {
            var newViewOldLeft = newView.left || 0;
            var previousViewOldLeft = previousView.left || 0;
            newView.left = $.mainWindow.size.width;
            newView.left = 319;
            $.content.add(newView);
            newView.animate({
                left: 0,
                duration: options.duration
            }, function() {
                newView.left = newViewOldLeft;
            });
            previousView.animate({
                left: -$.mainWindow.size.width,
                duration: options.duration
            }, function() {
                $.content.remove(previousView);
                previousView.left = previousViewOldLeft;
                exports.fireEvent("transitionend");
                callback && callback();
            });
        } else {
            var newViewOldLeft = newView.left || 0;
            newView.left = $.mainWindow.size.width;
            $.content.add(newView);
            newView.animate({
                left: 0,
                duration: options.duration
            }, function() {
                newView.left = newViewOldLeft;
                exports.fireEvent("transitionend");
                callback && callback();
            });
        }
    };
    $.transitions.slideInFromLeft = function(newView, previousView, options, callback) {
        exports.fireEvent("transitionstart");
        if (previousView) {
            var newViewOldLeft = newView.left || 0;
            var previousViewOldLeft = previousView.left || 0;
            newView.left = -$.mainWindow.size.width;
            $.content.add(newView);
            newView.animate({
                left: 0,
                duration: options.duration
            }, function() {
                newView.left = newViewOldLeft;
            });
            previousView.animate({
                left: $.mainWindow.size.width,
                duration: options.duration
            }, function() {
                $.content.remove(previousView);
                previousView.left = previousViewOldLeft;
                exports.fireEvent("transitionend");
                callback && callback();
            });
        } else {
            var newViewOldLeft = newView.left || 0;
            newView.left = -$.mainWindow.size.width;
            $.content.add(newView);
            newView.animate({
                left: 0,
                duration: options.duration
            }, function() {
                newView.left = newViewOldLeft;
                exports.fireEvent("transitionend");
                callback && callback();
            });
        }
    };
    $.transitions.none = function(newView, previousView, options, callback) {
        exports.fireEvent("transitionstart");
        if (previousView) {
            $.content.add(newView);
            $.content.remove(previousView);
            exports.fireEvent("transitionend");
            callback && callback();
        } else {
            $.content.add(newView);
            exports.fireEvent("transitionend");
            callback && callback();
        }
    };
    exports.clearContent = function() {
        for (var child in $.content.children) $.content.remove($.content.children[child]);
    };
    exports.hasHistory = function() {
        return $.prop.historyStack.length > 1 ? true : false;
    };
    exports.clearHistory = function(historyLimit) {
        historyLimit = historyLimit ? 1 > historyLimit ? 1 : historyLimit : 1;
        if ($.prop.historyStack.length > 1 && $.prop.historyStack.length > historyLimit) while ($.prop.historyStack.length > historyLimit) {
            var oldController = $.prop.historyStack.shift();
            var oldOptions = $.prop.historyStackOptions.shift();
            oldController.destroy();
            delete oldController;
            delete oldOptions;
        }
    };
    exports.bindBack = function() {
        $.mainWindow.addEventListener("androidback", exports.back);
    };
    exports.releaseBack = function() {
        $.mainWindow.removeEventListener("androidback", exports.back);
    };
    exports.exit = function() {
        $.mainWindow.close();
    };
    exports.getPrevious = function() {
        return $.previous;
    };
    exports.getPreviousController = function() {
        return $.previous.controller;
    };
    exports.getPreviousOptions = function() {
        return $.previous.options;
    };
    exports.getPreviousView = function() {
        return $.previous.view;
    };
    exports.getCurrent = function() {
        return $.current;
    };
    exports.getCurrentController = function() {
        return $.current.controller;
    };
    exports.getCurrentOptions = function() {
        return $.current.options;
    };
    exports.getCurrentView = function() {
        return $.current.view;
    };
    exports.addEventListener = function(eventName, action) {
        $.appWrap.addEventListener(eventName, action);
    };
    exports.removeEventListener = function(eventName, action) {
        $.appWrap.removeEventListener(eventName, action);
    };
    exports.fireEvent = function(eventName) {
        $.appWrap.fireEvent(eventName);
    };
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;