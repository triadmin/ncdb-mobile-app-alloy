function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "mask";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.winMask = Ti.UI.createWindow({
        backgroundColor: "#ccc",
        statusBarStyle: Titanium.UI.iPhone.StatusBar.LIGHT_CONTENT,
        opacity: "0.7",
        id: "winMask"
    });
    $.__views.winMask && $.addTopLevelView($.__views.winMask);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var ag = Alloy.Globals;
    $.winMask.addEventListener("click", function() {
        $.winMask.close();
        ag.modal.close(ag.slide_down_to_bottom);
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;