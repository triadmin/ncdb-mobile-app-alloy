function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "rowLibraryCats";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.tblRowLibraryCats = Ti.UI.createTableViewRow({
        height: 50,
        selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE,
        id: "tblRowLibraryCats"
    });
    $.__views.tblRowLibraryCats && $.addTopLevelView($.__views.tblRowLibraryCats);
    $.__views.__alloyId1 = Ti.UI.createView({
        id: "__alloyId1"
    });
    $.__views.tblRowLibraryCats.add($.__views.__alloyId1);
    $.__views.libraryCat = Ti.UI.createLabel({
        left: 12,
        font: {
            fontFamily: "Palatino-Roman",
            fontSize: 18
        },
        id: "libraryCat"
    });
    $.__views.__alloyId1.add($.__views.libraryCat);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    $.libraryCat.text = args.libraryCat || "";
    if (true == args.subCat) {
        var style = $.createStyle({
            classes: "table-row-sub-label"
        });
        $.libraryCat.applyProperties(style);
    }
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;