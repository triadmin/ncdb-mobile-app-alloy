function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "libraryList";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.libraryListWindow = Ti.UI.createView({
        id: "libraryListWindow"
    });
    $.__views.libraryListWindow && $.addTopLevelView($.__views.libraryListWindow);
    $.__views.headerLibrary = Alloy.createController("includes/headerLibrary", {
        id: "headerLibrary",
        __parentSymbol: $.__views.libraryListWindow
    });
    $.__views.headerLibrary.setParent($.__views.libraryListWindow);
    $.__views.tblLibraryList = Ti.UI.createTableView({
        top: 60,
        id: "tblLibraryList"
    });
    $.__views.libraryListWindow.add($.__views.tblLibraryList);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var ag = Alloy.Globals;
    var args = arguments[0] || {};
    var LibraryList = {};
    LibraryList.init = function() {
        libraryList = Alloy.createCollection("libraryList", {
            subCatId: args.subCatId
        });
        libraryList.fetch({
            success: function() {
                LibraryList.ui.renderRows();
            },
            error: function() {
                Ti.API.error("hmm - this is not good!");
            }
        });
    };
    LibraryList.ui = {
        renderRows: function() {
            var data = [];
            _.each(libraryList.models, function(element) {
                var a = element.attributes;
                var row = Alloy.createController("rowLibraryList", {
                    libraryTitle: a["LibraryTitle"],
                    libraryFormatIcon: a["LibraryFormatIcon"][0]
                }).getView();
                row.libraryItemURL = a["LibraryLink"];
                row.libraryItemEmailURL = a["LibraryLink"];
                if ("" != a["LibraryBody"]) {
                    row.libraryItemEmailURL = ag.site_url + "/library/page/" + a["LibraryId"];
                    var libraryBody = '<style>* { font-family: "Helvetica Neue"; } h1,h1 { font-family: "Times New Roman" }</style><h1>' + a["LibraryTitle"] + "</h1>" + a["LibraryBody"];
                }
                row.libraryContent = libraryBody;
                data.push(row);
            });
            $.tblLibraryList.setData(data);
            LibraryList.events.attachRowEvents();
        }
    };
    LibraryList.events = {
        attachRowEvents: function() {
            $.tblLibraryList.addEventListener("click", function(_e) {
                ag.navigation.open("libraryItemView", {
                    libraryItemURL: _e.row.libraryItemURL,
                    libraryContent: _e.row.libraryContent,
                    libraryItemEmailURL: _e.row.libraryItemEmailURL
                });
            });
        }
    };
    LibraryList.init();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;