function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "libraryItemView";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.libraryListWindow = Ti.UI.createView({
        id: "libraryListWindow"
    });
    $.__views.libraryListWindow && $.addTopLevelView($.__views.libraryListWindow);
    $.__views.headerLibrary = Alloy.createController("includes/headerLibrary", {
        id: "headerLibrary",
        __parentSymbol: $.__views.libraryListWindow
    });
    $.__views.headerLibrary.setParent($.__views.libraryListWindow);
    $.__views.webLibraryItem = Ti.UI.createWebView({
        top: 60,
        bottom: 40,
        id: "webLibraryItem"
    });
    $.__views.libraryListWindow.add($.__views.webLibraryItem);
    $.__views.footerLibrary = Alloy.createController("includes/footerLibrary", {
        id: "footerLibrary",
        __parentSymbol: $.__views.libraryListWindow
    });
    $.__views.footerLibrary.setParent($.__views.libraryListWindow);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var ag = Alloy.Globals;
    var args = arguments[0] || {};
    var LibraryView = {};
    LibraryView.init = function() {
        ag.ActiveLibraryURL = args.libraryItemEmailURL;
        "" != args.libraryItemURL ? $.webLibraryItem.url = args.libraryItemURL : $.webLibraryItem.html = args.libraryContent;
    };
    LibraryView.init();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;