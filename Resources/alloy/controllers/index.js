function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "index";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.mainWindow = Ti.UI.createWindow({
        backgroundColor: "white",
        statusBarStyle: Titanium.UI.iPhone.StatusBar.LIGHT_CONTENT,
        id: "mainWindow"
    });
    $.__views.mainWindow && $.addTopLevelView($.__views.mainWindow);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var navigation = Alloy.Globals.navigation = Alloy.createController("navigation");
    var conf = {
        mainWindow: void 0,
        index: "libraryCatsList",
        indexOptions: {
            transition: "none"
        },
        historyLimit: 20,
        defaultOpenTransition: {
            transition: "none",
            duration: 150,
            transitionColor: "#fff"
        },
        defaultBackTransition: {
            transition: "none",
            duration: 150,
            transitionColor: "#fff"
        },
        confirmOnExit: true
    };
    conf.defaultOpenTransition = {
        transition: "slideInFromRight",
        duration: 150,
        transitionColor: "#fff"
    };
    conf.defaultBackTransition = {
        transition: "slideInFromLeft",
        duration: 150,
        transitionColor: "#fff"
    };
    navigation.init(conf);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;