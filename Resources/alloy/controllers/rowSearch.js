function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "rowSearch";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.tblRowSearch = Ti.UI.createTableViewRow({
        height: 50,
        selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE,
        id: "tblRowSearch"
    });
    $.__views.tblRowSearch && $.addTopLevelView($.__views.tblRowSearch);
    $.__views.__alloyId3 = Ti.UI.createView({
        id: "__alloyId3"
    });
    $.__views.tblRowSearch.add($.__views.__alloyId3);
    $.__views.searchItem = Ti.UI.createLabel({
        left: 12,
        font: {
            fontFamily: "Palatino-Roman",
            fontSize: 18
        },
        id: "searchItem"
    });
    $.__views.__alloyId3.add($.__views.searchItem);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    $.searchItem.text = args.libraryTitle || "";
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;