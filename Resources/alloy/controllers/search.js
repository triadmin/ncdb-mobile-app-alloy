function Controller() {
    function closeSearchWindow() {
        $.searchWindow.close();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "search";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.searchWindow = Ti.UI.createWindow({
        backgroundColor: "white",
        statusBarStyle: Titanium.UI.iPhone.StatusBar.LIGHT_CONTENT,
        id: "searchWindow",
        modal: "true"
    });
    $.__views.searchWindow && $.addTopLevelView($.__views.searchWindow);
    $.__views.viewSearchBar = Ti.UI.createView({
        top: 0,
        width: "100%",
        height: 60,
        backgroundColor: "#e2dede",
        id: "viewSearchBar"
    });
    $.__views.searchWindow.add($.__views.viewSearchBar);
    $.__views.txtSearch = Ti.UI.createSearchBar({
        top: 18,
        left: 0,
        width: 250,
        borderWidth: 1,
        barColor: "#e2dede",
        borderColor: "#e2dede",
        id: "txtSearch",
        hintText: "Search"
    });
    $.__views.viewSearchBar.add($.__views.txtSearch);
    $.__views.lblClose = Ti.UI.createLabel({
        right: 10,
        top: 27,
        color: "#3a1f21",
        font: {
            fontFamily: "Helvetica Neue",
            fontSize: 16
        },
        text: "Done",
        id: "lblClose"
    });
    $.__views.viewSearchBar.add($.__views.lblClose);
    closeSearchWindow ? $.__views.lblClose.addEventListener("click", closeSearchWindow) : __defers["$.__views.lblClose!click!closeSearchWindow"] = true;
    $.__views.tblSearchResults = Ti.UI.createTableView({
        top: 60,
        id: "tblSearchResults"
    });
    $.__views.searchWindow.add($.__views.tblSearchResults);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var ag = Alloy.Globals;
    var Search = {};
    Search.init = function() {
        $.txtSearch.focus();
        var timer, lastSearch = null;
        $.txtSearch.addEventListener("change", function() {
            if ($.txtSearch.value.length > 2 && $.txtSearch.value != lastSearch) {
                clearTimeout(timer);
                timer = setTimeout(function() {
                    lastSearch = $.txtSearch.value;
                    Search.getData($.txtSearch.value);
                }, 300);
            }
            return false;
        });
        Search.events.attachRowEvents();
    };
    Search.getData = function(strSearch) {
        var AJAX = Ti.Network.createHTTPClient();
        AJAX.onload = function() {
            var jdata = JSON.parse(this.responseText);
            var data = [];
            _.each(jdata.data, function(element) {
                var row = Alloy.createController("rowSearch", {
                    libraryTitle: element.LibraryTitle
                }).getView();
                row.libraryItemURL = element.LibraryLink;
                if ("" != element.LibraryBody) var libraryBody = '<style>* { font-family: "Helvetica Neue"; } h1,h1 { font-family: "Times New Roman" }</style><h1>' + element.LibraryTitle + "</h1>" + element.LibraryBody;
                row.libraryContent = libraryBody;
                data.push(row);
            });
            $.tblSearchResults.setData(data);
        };
        AJAX.open("GET", ag.site_url + "/api/v1/library?search=" + strSearch + "&select=LibraryTitle,LibraryId,LibraryLink,LibraryBody");
        AJAX.send();
    };
    Search.events = {
        attachRowEvents: function() {
            $.tblSearchResults.addEventListener("click", function(_e) {
                closeSearchWindow();
                ag.navigation.open("libraryItemView", {
                    libraryItemURL: _e.row.libraryItemURL,
                    libraryContent: _e.row.libraryContent
                });
            });
        }
    };
    Search.init();
    __defers["$.__views.lblClose!click!closeSearchWindow"] && $.__views.lblClose.addEventListener("click", closeSearchWindow);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;