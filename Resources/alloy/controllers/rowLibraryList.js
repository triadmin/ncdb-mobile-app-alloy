function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "rowLibraryList";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.tblRowLibraryList = Ti.UI.createTableViewRow({
        height: 50,
        selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE,
        id: "tblRowLibraryList"
    });
    $.__views.tblRowLibraryList && $.addTopLevelView($.__views.tblRowLibraryList);
    $.__views.__alloyId2 = Ti.UI.createView({
        id: "__alloyId2"
    });
    $.__views.tblRowLibraryList.add($.__views.__alloyId2);
    $.__views.libraryItemIcon = Ti.UI.createLabel({
        left: 12,
        font: {
            fontFamily: Alloy.Globals.icon_font,
            fontSize: 14
        },
        color: "#bbb7b7",
        id: "libraryItemIcon"
    });
    $.__views.__alloyId2.add($.__views.libraryItemIcon);
    $.__views.libraryItem = Ti.UI.createLabel({
        left: 35,
        font: {
            fontFamily: "Palatino-Roman",
            fontSize: 16
        },
        id: "libraryItem"
    });
    $.__views.__alloyId2.add($.__views.libraryItem);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    $.libraryItem.text = args.libraryTitle || "";
    var iconStyle = $.createStyle({
        classes: args.libraryFormatIcon
    });
    $.libraryItemIcon.applyProperties(iconStyle);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;