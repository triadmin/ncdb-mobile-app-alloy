var Alloy = require("alloy"), _ = Alloy._, Backbone = Alloy.Backbone;

var app = {};

var fontawesome = require("IconicFont").IconicFont({
    font: "FontAwesome",
    ligature: false
});

Alloy.Globals.icon_font = fontawesome.fontfamily();

Alloy.Globals.icon_back = fontawesome.icon("icon-chevron-left");

Alloy.Globals.icon_search = fontawesome.icon("icon-search");

Alloy.Globals.icon_book = fontawesome.icon("icon-book");

Alloy.Globals.icon_play_circle = fontawesome.icon("icon-play-circle");

Alloy.Globals.icon_comment = fontawesome.icon("icon-comment");

Alloy.Globals.icon_desktop = fontawesome.icon("icon-desktop");

Alloy.Globals.icon_globe = fontawesome.icon("icon-globe");

Alloy.Globals.icon_share = fontawesome.icon("icon-share");

Alloy.Globals.TwitterConsumerSecret = "l183kstis72xlWdJkEgYAG4W0Ffli7PWyxZJjRqdkw";

Alloy.Globals.TwitterConsumerKey = "krROZoQVQdrZ1L6eKgdoDA";

Alloy.Globals.mask = Alloy.createController("mask").getView();

Alloy.Globals.modal = Alloy.createController("modalShare").getView();

Alloy.Globals.slide_up_from_bottom = Ti.UI.createAnimation({
    bottom: 0,
    duration: 300
});

Alloy.Globals.slide_down_to_bottom = Ti.UI.createAnimation({
    bottom: -200,
    duration: 300
});

Alloy.Globals.site_url = "http://nationaldb.org";

app.openSearch = function() {
    Alloy.createController("search").getView().open();
};

app.modalControl = {
    openShare: function() {
        Alloy.Globals.mask.open();
        Alloy.Globals.modal.open(Alloy.Globals.slide_up_from_bottom);
    }
};

app.goBack = function() {
    Alloy.Globals.navigation.back();
};

Alloy.createController("index");